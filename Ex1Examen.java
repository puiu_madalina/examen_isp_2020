package ex1examen;
/**
 *
 * @author Madalina
 */
class A {
}

class B extends A {

    D d;

    private long t;

    public void b(C c) {
    }
}

interface Y {

}

class F {
}

class E {

    public void met2() {
        System.out.println("met2");
    }
}

class D implements Y {

    private F f;
    private E e;

    public D(F f) {
        this.f = f;
        e = new E();
    }

    public void met1(int i) {
        System.out.println("met1 " + i);
    }
}

class C {
}
