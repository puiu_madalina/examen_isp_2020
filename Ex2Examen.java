package ex2examen;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JTextField;
/**
 *
 * @author Madalina
 */
public class Ex2Examen {

    public static void main(String[] args) {
        new MyFrame();
    }
}

class MyFrame extends JFrame implements ActionListener {

    private JTextField text1, text2;
    private JButton buton;

    public MyFrame() {
        setSize(240, 300);
        setTitle("Text");
        setLayout(null);
        setResizable(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        text1 = new JTextField();
        text1.setBounds(10, 50, 200, 30);

        text2 = new JTextField();
        text2.setBounds(10, 100, 200, 30);
        text2.setEditable(false);

        buton = new JButton("copy");
        buton.setBounds(10, 150, 200, 30);
        buton.addActionListener(this);

        add(text1);
        add(text2);
        add(buton);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String s = text1.getText();
        text2.setText(s);
    }

}
